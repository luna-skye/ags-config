import GLib from 'gi://GLib'

const hyprland = await Service.import('hyprland') // hyprland stuff
const mpris = await Service.import('mpris') // media players
const audio = await Service.import('audio') // audio system stuff
const systemtray = await Service.import('systemtray') // systray stuff


// variables
const time = Variable('', { poll: [ 1000, "date '+%T'" ] })
const date = Variable('', { poll: [ 60000, "date '+%B %d, %Y (%A)'" ] })


// screen corner widgets
const corner = (monitor = 0) => Widget.Window({
  monitor,
  name: `corner_${monitor}`,
  class_name: 'screen-corner corners',
  anchor: [ 'top', 'bottom', 'right', 'left' ],
  click_through: true,
  child: Widget.Box({
    class_name: 'border',
    expand: true,
    child: Widget.Box({
      class_name: 'corner',
      expand: true,
    }),
  }),
  // setup: self => self.hook(corners, () => {
  //   self.toggleClassName('corners', corners.value)
  // })
})


// bar widget
const bar = (monitor = 0) => Widget.Window({
  monitor,
  name: `bar_${monitor}`,
  class_name: 'bar',
  exclusivity: 'exclusive',
  anchor: [ 'top', 'left', 'right' ],
  child: Widget.CenterBox({
    // LEFT
    start_widget: Widget.Box({
      spacing: 8,
      children: [
        launcher(),
        workspaces(monitor),
      ]
    }),

    // CENTER
    center_widget: clock(),

    // RIGHT
    end_widget: Widget.Box({
      hpack: 'end',
      spacing: 8,
      children: [
        volume(),
        systray(),
      ]
    })
  })
})


// app launcher button
const launcher = () => {
  return Widget.Button({
    class_name: 'launcher',
    child: Widget.Overlay({
      'pass-through': true,
      overlays: [
        Widget.Icon({
          hpack: 'center',
          vpack: 'center',
          size: 96,
          icon: '/home/skye/.config/ags/logo.png',
        }) 
      ],
      child: Widget.Box({
        child: Widget.Box({ class_name: 'internal' })
      }),
    }),
    on_clicked: () => Utils.exec(`tofi-drun | xargs hyprctl dispatch exec --`)
  })
}


// workspaces widget
const workspaces = (monitor = 0) => {
  const activeID = hyprland.active.workspace.bind('id')

  const workspaces = hyprland.bind('workspaces').as(ws => {
    return ws.sort((a, b) => a.id - b.id).map(({ id, monitorID }) => {
      // return empty box if not applicable to this monitor
      if (monitorID !== monitor) return Widget.Box()

      // return button widget with bindings for hyprland workspaces
      return Widget.Button({
        class_name: activeID.as(i => `${i === id ? 'active' : ''}`),
        child: Widget.Label(`${id}`),
        on_clicked: () => hyprland.messageAsync(`dispatch workspace ${id}`),
      })
    })
  })

  return Widget.Box({
    class_name: 'workspaces',
    children: workspaces
  })
}


// clock widget
const clock = () => {
  return Widget.Box({
    class_name: 'clock',
    spacing: 8,
    children: [
      Widget.Label({ label: time.bind(), class_name: 'time' }),
      Widget.Label({ label: date.bind() }),
    ]
  })
}


// media player
const media = () => {
  const playIcon = Utils.watch(
    '',
    mpris,
    'player-changed',
    () => {
      const { play_back_status } = mpris.players[0]
      switch (play_back_status) {
        case "Playing": return 'media-playback-pause-symbolic'
        case "Paused": return 'media-playback-start-symbolic'
        case "Stopped": return 'media-playback-start-symbolic'
        default: return ''
      }
    }
  )

  const label = Utils.watch(Widget.Label(), mpris, 'player-changed', () => {
    if (mpris.players[0]) {
      const { track_artists, track_title } = mpris.players[0]

      return Widget.Box({
        spacing: 0,
        hpack: 'end',
        vertical: true,
        children: [
          Widget.Label({
            class_name: 'title',
            hpack: 'start',
            justification: 'right',
            truncate: 'end',
            max_width_chars: 24,
            label: `${track_title}`,
          }),
          Widget.Label({
            class_name: 'artist',
            hpack: 'start',
            justification: 'right',
            truncate: 'end',
            max_width_chars: 24,
            label: `${track_artists.join(', ')}`,
          }),
        ]
      })
    } else {
      return Widget.Label('Nothing is playing')
    }
  })

  return Widget.Box({
    class_name: 'media',
    spacing: 8,
    children: [
      Widget.Button({
        class_name: 'play-pause',
        child: Widget.Icon({ icon: playIcon }),
        on_primary_click: () => mpris.getPlayer('')?.playPause(),
      }),
      Widget.Button({ child: label })
    ]
  })
}


// volume slider widget
const volume = () => {
  // get volume icon
  const icons = {
    101: 'overamplified',
    67: 'high',
    34: 'medium',
    1: 'low',
    0: 'muted'
  }
  const getIcon = () => {
    const icon = audio.speaker.is_muted ? 0 : [101, 67, 34, 1, 0].find(
      threshold => threshold <= audio.speaker.volume * 100)
    return `audio-volume-${icons[icon]}-symbolic`
  }
  const icon = Widget.Button({
    on_primary_click: () => audio.speaker.is_muted = !audio.speaker.is_muted,
    child: Widget.Icon({ icon: Utils.watch(getIcon(), audio.speaker, getIcon) })
  })

  // define volume slider
  const slider = Widget.Slider({
    hexpand: true,
    draw_value: false,
    on_change: ({ value }) => audio.speaker.volume = value,
    setup: self => self.hook(audio.speaker, () => self.value = audio.speaker.volume || 0)
  })

  // return the widget box with icon & slider
  return Widget.Box({
    spacing: 8,
    class_name: 'volume',
    css: 'min-width: 128px',
    children: [ icon, slider, media() ],
  })
}


// system metric
const metric = () => {
  return Widget.Box({
    spacing: 8,
    children: []
  })
}


// systray
const systray = () => {
  const items = systemtray.bind('items')
    .as(items => items.map(item => Widget.Button({
      child: Widget.Icon({ icon: item.bind("icon") }),
      // on_primary_click: (_, e) => item.activate(e),
      on_secondary_click: (_, e) => item.openMenu(e),
      tooltip_markup: item.bind('tooltip_markup')
    })))
  return Widget.Box({
    class_name: 'systray',
    children: items
  })
}


// auto-reload scss stuff
function loadCSS() {
  const scss = `${App.configDir}/style.scss`
  const css = '/tmp/ags-style.css'
  Utils.exec(`sassc ${scss} ${css}`)
  App.resetCss()
  App.applyCss(css)
}
Utils.monitorFile(
  `${App.configDir}/style.scss`,
  function() { loadCSS() }
)
loadCSS()

// app config
App.config({
  windows: [
    bar(0),
    bar(1),
    corner(0),
    corner(1),
  ]
})
