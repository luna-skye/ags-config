# luna skye's AGS (Aylur's GTK Shell) widgets

these are the dotfile configurations for my AGS widget system, intended for use with my [nixos dotfiles](https://gitlab.com/luna-skye/dotfiles)

it may be a bit messy while i learn AGS, and i wouldn't recommend using these until well after this readme has changed.
